package com.maequise;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This is a servlet used in the second page JSP
 * In this one we return the JSP page directly, no PrintWriter is used
 *
 * @author maequise
 * @version 1.0.0
 * @since 1.0.0
 */
public class PageServlet extends HttpServlet {
    /**
     * This method handles the GET method of the client
     * @param req the HTTP request made on the server
     * @param resp the HTTP response returned by the server
     * @throws ServletException if any error occurs during the treatment
     * @throws IOException if we used the PrintWriter to write on the page
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("title", "this is a title !");

        req.getRequestDispatcher("/page.jsp").forward(req, resp);
    }

    /**
     * This method handles the POST method of the client
     * @param req the HTTP request made on the server
     * @param resp the HTTP response returned by the server
     * @throws ServletException if any error occurs during the treatment
     * @throws IOException if we used the PrintWriter to write on the page
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
