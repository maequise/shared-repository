package com.maequise;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * This is a servlet used in the home page, and when we submit the form
 *
 * @author maequise
 * @version 1.0.0
 * @since 1.0.0
 */
public class ServletListener extends HttpServlet {
    /**
     * This method handles the GET method of the client
     * @param req the HTTP request made on the server
     * @param resp the HTTP response returned by the server
     * @throws ServletException if any error occurs during the treatment
     * @throws IOException if we used the PrintWriter to write on the page
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("text/html");
        PrintWriter print = resp.getWriter();

        print.write("this is a print ! \n");

        req.setAttribute("text", "this is a title ! <br />");

        print.write(req.getAttribute("text").toString());

        print.write("Here's the username "+ req.getParameter("user"));
        print.close();
    }

    /**
     * This method handles the POST method of the client
     * @param req the HTTP request made on the server
     * @param resp the HTTP response returned by the server
     * @throws ServletException if any error occurs during the treatment
     * @throws IOException if we used the PrintWriter to write on the page
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String user = req.getParameter("user");

        doGet(req, resp);
    }
}
